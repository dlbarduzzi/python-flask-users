# python-flask-users

[![pipeline status](https://gitlab.com/dlbarduzzi/python-flask-users/badges/main/pipeline.svg)](https://gitlab.com/dlbarduzzi/python-flask-users/commits/main)


### Commands

Running the app locally.

```sh
export FLASK_ENV=development

# This one gets deleted at some point when working with docker
export FLASK_APP=src/__init__.py

# May not be necessary if not using settings object yet
export APP_SETTINGS=src.config.DevelopmentConfig

python manage.py run
```

Running the app with docker.

```sh
docker-compose build
docker-compose up -d
```

Rebuild and run the docker app.

```sh
docker-compose up -d --build
```

Running tests.

```sh
docker-compose up -d --build
docker-compose exec api python -m pytest "src/tests"

# force build
docker-compose build --no-cache

# To select specific tests to run, you can use a substring matching.
docker-compose exec api python -m pytest "src/tests" -k config

# Only re-run tests that failed from a previous run
docker-compose exec api python -m pytest "src/tests" --lf
```

Create the database.

```sh
docker-compose exec api python manage.py recreate_db
docker-compose exec api-db psql -U postgres

postgres=# \c api_dev
You are now connected to database "api_dev" as user "postgres".

api_dev=# \dt
Did not find any relations.

api_dev=# \q
```

Generate database data.

```sh
docker-compose exec api python manage.py seed_db
```


### Extra

Want to view app configuration? Add the following lines after initiation the Flask app and its configurations.

```python
import sys
print(app.config, file=sys.stderr)
```

### Full down and up/test commands

```sh
docker-compose down -v
docker-compose up -d --build
docker-compose exec api python manage.py recreate_db
docker-compose exec api python manage.py seed_db

docker-compose exec api python -m pytest "src/tests" -p no:warnings
docker-compose exec api flake8 src
docker-compose exec api black src
docker-compose exec api isort src
```
