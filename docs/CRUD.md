# crud actions

```sh
# get all users
curl -s -X GET https://secure-inlet-65038.herokuapp.com/users | jq

# get a single user
curl -s -X GET https://secure-inlet-65038.herokuapp.com/users/1 | jq

# create a new user
curl https://secure-inlet-65038.herokuapp.com/users \
  -X POST \
  -H "Content-Type: application/json" \
  --data '
{
  "username": "new_user",
  "email": "new_user@example.com"
}
'

# update user
curl https://secure-inlet-65038.herokuapp.com/users/4 \
  -X PUT \
  -H "Content-Type: application/json" \
  --data '
{
  "username": "new_user.test",
  "email": "new_user.test@example.com"
}
'

# delete user
curl -X DELETE https://secure-inlet-65038.herokuapp.com/users/4
```
