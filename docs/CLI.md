# CLI

### pytest

```sh
# normal run
$ docker-compose exec api python -m pytest "src/tests"

# disable warnings
$ docker-compose exec api python -m pytest "src/tests" -p no:warnings

# run only the last failed tests
$ docker-compose exec api python -m pytest "src/tests" --lf

# run only the tests with names that match the string expression
$ docker-compose exec api python -m pytest "src/tests" -k "config and not test_development_config"

# stop the test session after the first failure
$ docker-compose exec api python -m pytest "src/tests" -x

# enter PDB after first failure then end the test session
$ docker-compose exec api python -m pytest "src/tests" -x --pdb

# stop the test run after two failures
$ docker-compose exec api python -m pytest "src/tests" --maxfail=2

# show local variables in tracebacks
$ docker-compose exec api python -m pytest "src/tests" -l

# list the 2 slowest tests
$ docker-compose exec api python -m pytest "src/tests" --durations=2

# run the tests with coverage
docker-compose exec api python -m pytest "src/tests" -p no:warnings --cov="src"

# run the tests with coverage and generate an HTML report of the results:
docker-compose exec api python -m pytest "src/tests" -p no:warnings --cov="src" --cov-report html
open htmlcov/index.

# run UNIT tests in parallel, since they don't need order (i.e. they don't access the database)
docker-compose exec api python -m pytest "src/tests/unit/test_users_unit.py" -p no:warnings -k "unit" -n auto
```

### heroku

```sh
# create a new app
heroku create

# login to heroku container registry
heroku container:login

# provision a new Postgres database with the hobby-dev plan
# secure-inlet-65038 is the app name provided from the output when running `heroku create`
heroku addons:create heroku-postgresql:hobby-dev --app secure-inlet-65038

# create docker image with heroku registry
docker build -f Dockerfile.prod -t registry.heroku.com/secure-inlet-65038/web .

# run image locally
docker run --name flask-users -e "PORT=8765" -p 5005:8765 registry.heroku.com/secure-inlet-65038/web:latest

# bring down the container
docker rm flask-users

# push image to heroku container registry
docker push registry.heroku.com/secure-inlet-65038/web:latest

# release the image
heroku container:release web --app secure-inlet-65038

# https://secure-inlet-65038.herokuapp.com/ping

# view logs
heroku logs --app secure-inlet-65038

# create the database and then seed the db
heroku run python manage.py recreate_db --app secure-inlet-65038
heroku run python manage.py seed_db --app secure-inlet-65038

# create a token
heroku auth:token

# exec (ssh) into the app/container
heroku run bash --app secure-inlet-65038
export FLASK_APP=src/__init__.py
flask shell
app.config["SECRET_KEY"]
```


### flake8

```sh
docker-compose exec api flake8 src
```


### black

```sh
# check results, see what files would be changed
docker-compose exec api black src --check

# check the difference black would make
docker-compose exec api black src --diff

# apply changes
docker-compose exec api black src
```


### isort

```sh
# check results, see what files would be changed
docker-compose exec api isort src --check-only

# check the difference isort would make
docker-compose exec api isort src --diff

# apply changes
docker-compose exec api isort src
```
